﻿using Galaga.Scripts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Galaga
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Game : Page
    {
        #region Player Data
        const string PlayerSpriteSource = "shipweenie.png";
        const double PlayerMoveSpeed = 500;
        const double PlayerWidth = 46;
        const double PlayerHeight = 56;
        const double PlayerStartY = 0;
        #endregion

        #region Update Timer
        DispatcherTimer updateTimer;
        DateTimeOffset lastUpdateTime;
        const int updateIntervalMilliseconds = 10;

        #endregion

        public static Game Instance { get; private set; }

        public PhysicsEngine Physics { get; private set; }

        public Action<double> Update { get; set; }

        public Grid Grid { get { return GameGrid; } }
        public EntityFactory EntityFactory {get; private set;}
        public WaveManager WaveManager { get; set; }

        public Character player { get; private set; }
        Character AI;

        public Game()
        {
            Instance = this;
            Physics = new PhysicsEngine();
            EntityFactory = new EntityFactory();
            this.InitializeComponent();
            WaveManager = new WaveManager();
        }

        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            double screenCenter = RenderSize.Width / 2;
            double playerStartX = screenCenter - PlayerWidth / 2;

            Sprite PlayerSprite = new Sprite(PlayerWidth, PlayerHeight, PlayerSpriteSource, playerStartX, PlayerStartY, 0);
            CharacterController playerController = new PlayerController(PlayerMoveSpeed);
            player = new Character(PlayerSprite, playerController);

            WaveManager.Initialize("rapesack", null);

            InitializeUpdateTimer();
        }

        void InitializeUpdateTimer()
        {
            updateTimer = new DispatcherTimer();
            updateTimer.Interval = TimeSpan.FromMilliseconds(updateIntervalMilliseconds);
            updateTimer.Tick += OnUpdateTimerTick;

            lastUpdateTime = DateTimeOffset.Now;

            updateTimer.Start();
        }

        private void OnUpdateTimerTick(object sender, object e)
        {
            //Calculate elapsed time
            DateTimeOffset currentTime = DateTimeOffset.Now;

            TimeSpan elapsedTime = (currentTime - lastUpdateTime);

            double dt = elapsedTime.TotalSeconds;

            if (Update != null)
                Update(dt);

            lastUpdateTime = currentTime;

//            GameGrid.Children.Remove(Bullet bullet);
        }
    }
}
