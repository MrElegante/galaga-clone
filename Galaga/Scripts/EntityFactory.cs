﻿using Galaga.Scripts;
using Galaga.Scripts.Ai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class EntityFactory
    {
        public void SpawnEntity(char entityCode, double posX, double posY)
        {
            if (entityCode == '0')
                return;

            switch(entityCode)
            {
                case '0':
                    break;
                case 'A':
                    SpawnAI(entityCode, posX, posY, 46, 56, 160, 0.25, "ship.png");
                    break;
                case 'B':
                    SpawnAI(entityCode, posX, posY, 46, 56, 160, 0.25, "ship.png");
                    break;
                case 'C':
                    SpawnAI(entityCode, posX, posY, 46, 56, 160, 0.25, "ship.png");
                    break;
                case 'D':
                    SpawnAI(entityCode, posX, posY, 46, 56, 160, 0.25, "ship.png");
                    break;
                default:
                    break;
            }
        }

        Character SpawnAI(char entityCode, double posX, double posY, double width, double height, double movespeed, double fireRate, string spriteSource)
        {
            Sprite AISprite = new Sprite(width, height, spriteSource, posX, posY, 180);
            CharacterController AIController = SpawnAICharacterController(entityCode, movespeed);
            return new Character(AISprite, AIController);
        }

        CharacterController SpawnAICharacterController(char entityCode, double moveSpeed)
        {
            switch(entityCode)
            {
                case 'A':
                    return new AiCircleCharacterController(13, 100, true);
                case 'B':
                    return new AiCircleCharacterController(13, 200, false);
                case 'C':
                    return null; //return pingpong controller
                case 'D':
                    return null; // return pingpong controller
                default:
                    throw new Exception("Character controller not defined for entity " + entityCode);

            }
        }
    }
}
