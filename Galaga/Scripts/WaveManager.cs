﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class WaveManager
    {
        const double cellWidth = 100;
        const double cellHeight = 100;
        public void Initialize(string waveName, Action onWaveComplete)
        {
            string[] rows = FetchWaveData(waveName).Split('\n');
            for (int row = 0; row < rows.Length; row++)
            {
                SpawnRow(rows[row], row);
            }
        }

        void SpawnRow(string rowData, int rowIndex)
        {
            for (int column = 0; column< rowData.Length; column++)
            {
                double posX = column * cellWidth;
                double posY = - Game.Instance.RenderSize.Height + ((rowIndex+1) * cellHeight);
                Game.Instance.EntityFactory.SpawnEntity(rowData[column], posX, posY);
            }
        }

        string FetchWaveData(string waveName)
        {
            return "AAA000000\n" +
                   "000000BBB\n" + 
                   "000000000\n";

        }
    }
}
