﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class Bullet : GameEntity
    {
        Character ownerCharacter;
//        Sprite BulletSprite;
        const string BulletSpriteSource = "Bullet.png";
        const double BulletMoveSpeed = 300;
        const double BulletWidth = 9;
        const double BulletHeight = 48;
        double rotation;
        double moveDirectionX;
        double moveDirectionY;


        public Bullet(Character ownerCharacter) : base(GetBulletSprite(ownerCharacter), true)
        {
            this.ownerCharacter = ownerCharacter;
            this.rotation = ownerCharacter.Sprite.Rotation;
            double radians = (Math.PI / 180) * (this.rotation);
            moveDirectionX = Math.Sin(radians);
            moveDirectionY = Math.Cos(radians);
        }

        protected override void Update(double dt)
        {
            base.Update(dt);
            this.Sprite.PositionY -= ((BulletMoveSpeed * (dt)) * moveDirectionY);
            this.Sprite.PositionX += ((BulletMoveSpeed * (dt)) * moveDirectionX);

//            this.Sprite.PositionY -= ((ownerCharacter.Sprite.PositionX - ownerCharacter.Sprite.Width) / this.Sprite.Height * (BulletMoveSpeed * (dt)));
//            this.Sprite.PositionX += (this.Sprite.Height - this.Sprite.Width * (BulletMoveSpeed * (dt) / ));

            //try to destroy bullet if it leaves the screen here
            if (this.Sprite.PositionY < (-Game.Instance.RenderSize.Height+100) || (this.Sprite.PositionY > -45) || (this.Sprite.PositionX > (Game.Instance.RenderSize.Width)) || (this.Sprite.PositionX < 0))
            {
                Game.Instance.Update -= Update;
                this.Sprite.DestroySprite();
            }
        }

        static Sprite GetBulletSprite(Character ownerCharacter)
        {
            double spawnX = ownerCharacter.Sprite.PositionX + ownerCharacter.Sprite.Width / 2 - BulletWidth / 2;
            double spawnY = ownerCharacter.Sprite.PositionY - ownerCharacter.Sprite.Height - 1;
            return new Sprite(BulletWidth, BulletHeight, BulletSpriteSource, spawnX, spawnY, ownerCharacter.Sprite.Rotation);
        }

        bool ShouldDestroyFromCollision(GameEntity other)
        {
            if (other == ownerCharacter)
                return false;

            if (other is Bullet)
                return false;

            return true;
        }

        protected override void OnCollision(GameEntity other)
        {
            base.OnCollision(other);
            
            if(ShouldDestroyFromCollision(other))
            {
                Game.Instance.Update -= Update;
                this.Sprite.DestroySprite();
            }

        }
    }
}
