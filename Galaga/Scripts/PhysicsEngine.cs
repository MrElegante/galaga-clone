﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class PhysicsEngine
    {
        HashSet<Collider> colliders = new HashSet<Collider>();

        public PhysicsEngine()
        {
            Game.Instance.Update += Update;
        }

        private void Update(double dt)
        {
            for (int i = 0; i < colliders.Count; i++)
            {
                Collider a = colliders.ElementAt(i);
                if (a.sprite.ToDelete == true)
                {
                    UnregisterCollider(a);
                }
                for (int j = i+1; j < colliders.Count; j ++)
                {
                    Collider b = colliders.ElementAt(j);

                    if (Collider.HasCollision(a, b) == true)
                    {
                        a.TriggerCollision(b);
                        b.TriggerCollision(a);
                    }
                }
            }
        }

        public void RegisterCollider(Collider collider)
        {
            colliders.Add(collider);
        }

        public void UnregisterCollider(Collider collider)
        {
            colliders.Remove(collider);
        }
    }
}
