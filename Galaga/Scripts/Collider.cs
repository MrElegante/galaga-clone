﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class Collider
    {
        public double TopY { get { return sprite.PositionY - sprite.Height; } }
        public double BotY { get { return sprite.PositionY; } }
        public double LeftX { get { return sprite.PositionX; } }
        public double RightX { get { return sprite.PositionX + sprite.Width; } }
        public Sprite sprite { get; private set; }

        public Action<Collider> OnCollision { get; set; }
        public object Owner { get; private set; }

        public Collider(object owner, Sprite sprite)
        {
            this.Owner = owner;
            this.sprite = sprite;
            Game.Instance.Physics.RegisterCollider(this);
        }

        public void TriggerCollision(Collider other)
        {
            if (OnCollision != null)
                OnCollision(other);
        }

        public static bool HasCollision(Collider a, Collider b)
        {
            //if below: no collision
            if (a.TopY > b.BotY)
                return false;

            //if above: no collision
            if (a.BotY < b.TopY)
                return false;

            //if to right: no collision
            if (a.LeftX > b.RightX)
                return false;

            //if to left: no collision
            if (a.RightX < b.LeftX)
                   return false;

            //if none of above: collision
            return true;

        }
    }
}
