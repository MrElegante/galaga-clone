﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using System.Diagnostics;
using Windows.System;
using Windows.Foundation;

namespace Galaga.Scripts
{
    public class PlayerController : CharacterController
    {
        double fireDelay = 0.15;
        double fireCooldown = 0;
        //        double moveCooldown = 0;
        //        double moveDelay = 0.01;

        const VirtualKey MoveLeftKey = VirtualKey.Left;
        const VirtualKey MoveRightKey = VirtualKey.Right;
        const VirtualKey FireKey = VirtualKey.Space;
        const VirtualKey RotateRightKey = VirtualKey.D;
        const VirtualKey RotateLeftKey = VirtualKey.A;


        public PlayerController(double moveSpeed) : base(moveSpeed)
        {
            Window.Current.CoreWindow.KeyDown += OnKeyDown;
            Window.Current.CoreWindow.KeyUp += OnKeyUp;
        }

        private void OnKeyUp(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.KeyEventArgs e)
        {

            if ((CurrentMoveDirection.X == -1 && e.VirtualKey == MoveLeftKey)
                || (CurrentMoveDirection.X == 1 && e.VirtualKey == MoveRightKey))
            {
                CurrentMoveDirection = new Point(0, 0);
                character.StopTurning();
            }

        }

        private void OnKeyDown(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.KeyEventArgs e)
        {
            switch (e.VirtualKey)
            {
                case MoveLeftKey:
                    character.TurnLeft();
                    CurrentMoveDirection = new Point(-1,0);
                    break;
                case MoveRightKey:
                    character.TurnRight();
                    CurrentMoveDirection = new Point(1,0);
                    break;
                case FireKey:
                    character.Fire();
                    fireCooldown -= fireDelay;
                    break;
                case RotateRightKey:
                    character.Sprite.RotateSprite(10);
                    break;
                case RotateLeftKey:
                    character.Sprite.RotateSprite(-10);
                    break;
            }
        }


    }
}
