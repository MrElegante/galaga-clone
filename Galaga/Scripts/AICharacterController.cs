﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public class AICharacterController : CharacterController
    {
        static Random randomMove = new Random();
        double moveDelay = 1;
        double moveCooldown = 0;
        int moveInt;
        protected AiPattern AiPattern;
        bool hasHitCheckpoint1;
        double scriptStartingX = -1;

        public AICharacterController(double moveSpeed) : base(moveSpeed)
        {
            randomMoveDirection();
        }

        protected override void Update(double dt)
        {
            base.Update(dt);
            moveCooldown += dt;
            if (moveCooldown >= moveDelay)
            {
                moveCooldown = 0;
            }

        }

        void randomMoveDirection()
        {
            if (this.character != null)
            {
                moveInt = randomMove.Next(1, 3);
                switch (moveInt)
                {
                    case 1:
                        AiPattern = AiPattern.PingPongL;
                        //CurrentMoveDirection = MoveDirection.Left;
                        break;
                    case 2:
                        AiPattern = AiPattern.PingPongR;
                        break;
                    case 3:
                        character.Fire();
                        break;
                    case 4:
                        character.Sprite.RotateSprite(5);
                        break;
                    case 5:
                        character.Sprite.RotateSprite(-5);
                        break;
                    case 6:
                        aimAtPlayer();
                        character.Fire();
                        break;
                }

            }
        }

        void moveAILeft(double duration)
        {

        }

        void aimAtPlayer()
        {
            double distanceFromPlayerX = (Game.Instance.player.Sprite.PositionX - character.Sprite.PositionX);
            double distanceFromPlayerY = (Game.Instance.player.Sprite.PositionY + Game.Instance.player.Sprite.Height / 2) + (character.Sprite.PositionY - character.Sprite.Height - 1);
            double radian = (Math.PI / 180) * (distanceFromPlayerX);
            character.Sprite.ResetSpriteRotation();
            character.Sprite.RotateSprite(-(Math.Atan2(distanceFromPlayerY, distanceFromPlayerX) * 180.0 / Math.PI - 90));
            character.Fire();
        }

        //void PingPongR(double distance)
        //{
        //    if (scriptStartingX == -1)
        //        scriptStartingX = this.character.Sprite.PositionX;

        //    if ((this.character.Sprite.PositionX < scriptStartingX + distance && hasHitCheckpoint1 != true))
        //    {
        //        CurrentMoveDirection = MoveDirection.Right;
        //    }
        //    else
        //        hasHitCheckpoint1 = true;

        //    if (this.character.Sprite.PositionX == maxCharacterRight && hasHitCheckpoint1 != true)
        //        hasHitCheckpoint1 = true;

        //        if (this.character.Sprite.PositionX > scriptStartingX && hasHitCheckpoint1 == true)
        //    {
        //        CurrentMoveDirection = MoveDirection.Left;
        //    }

        //    if (this.character.Sprite.PositionX <= scriptStartingX && hasHitCheckpoint1 == true)
        //    {
        //        CurrentMoveDirection = MoveDirection.None;
        //        AiPattern = AiPattern.None;
        //        hasHitCheckpoint1 = false;
        //        scriptStartingX = -1;
        //    }
        //}

        //void PingPongL(double distance)
        //{
        //    if (scriptStartingX == -1)
        //        scriptStartingX = this.character.Sprite.PositionX;

        //    if ((this.character.Sprite.PositionX > scriptStartingX - distance && hasHitCheckpoint1 != true))
        //    {
        //        CurrentMoveDirection = MoveDirection.Left;
        //    }
        //    else
        //        hasHitCheckpoint1 = true;

        //    if (this.character.Sprite.PositionX == 0 && hasHitCheckpoint1 != true)
        //        hasHitCheckpoint1 = true;

        //    if (this.character.Sprite.PositionX < scriptStartingX && hasHitCheckpoint1 == true)
        //    {
        //        CurrentMoveDirection = MoveDirection.Right;
        //    }
        //    if ((this.character.Sprite.PositionX >= scriptStartingX && hasHitCheckpoint1 == true) || (this.character.Sprite.PositionX ==0 && hasHitCheckpoint1 == true))
        //    {
        //        CurrentMoveDirection = MoveDirection.None;
        //        AiPattern = AiPattern.None;
        //        hasHitCheckpoint1 = false;
        //        scriptStartingX = -1;
        //    }
        //}


    }
}
