﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public abstract class GameEntity
    {
        public Sprite Sprite { get; set; }
        public Game Game { get; }

        readonly Collider collider;
        

        public GameEntity(Sprite sprite, bool hasCollider)
        {
            Sprite = sprite;
            Game.Instance.Update += Update;

            if(hasCollider)
            {
                collider = new Collider(this, Sprite);
                collider.OnCollision += OnColliderCollision;
            }

        }

        ~GameEntity()
        {
            if (Game.Instance != null)
            {
                Game.Instance.Update -= Update;

            }
        }

        protected virtual void Update(double dt)
        {
            
        }

        protected virtual void OnCollision(GameEntity other)
        {

        }

        private void OnColliderCollision(Collider other)
        {
            //If this collision was with another GameEntity, call OnCollision
            GameEntity otherEntity = other.Owner as GameEntity;

            if (otherEntity != null)
            {
                OnCollision(otherEntity);
            }
        }
    }
}
