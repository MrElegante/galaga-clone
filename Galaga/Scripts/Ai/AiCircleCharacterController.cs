﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Galaga.Scripts.Ai
{
    public class AiCircleCharacterController : CharacterController
    {
        private readonly double radius;
        private readonly bool isClockwise;
        double moveSpeed;

        double time; //variable that stores how much time has passed since spawn. Used for moving around in a circle
        Point orbitPoint;

        public AiCircleCharacterController(double moveSpeed, double radius, bool isClockwise) : base(moveSpeed)
        {
            this.radius = radius;
            this.isClockwise = isClockwise;
            this.moveSpeed = moveSpeed;
        }

        public override void Initialize(Character character)
        {
            base.Initialize(character);

            orbitPoint = new Point(character.Sprite.PositionX, character.Sprite.PositionY);
            orbitPoint.Y += radius; // cet the center of the orbit down radius from start point
            time = 0;
        }

        protected override void UpdateMove(double dt)
        {

            time += dt;

            int clocwiseflip = isClockwise ? 1 : -1;

            double degreToOrbitPoint = (-(Math.Atan2(-(orbitPoint.Y - character.Sprite.PositionY), (orbitPoint.X - character.Sprite.PositionX)) * 180.0 / Math.PI - 90)) * clocwiseflip;
            double radians = (Math.PI / 180) * (degreToOrbitPoint - 90);
            double posX = Math.Max(5, character.Sprite.PositionX + (((Math.Sin(radians)) * clocwiseflip) * moveSpeed));
            double posY = character.Sprite.PositionY - (((Math.Cos(radians))* moveSpeed));



            SetPositionX(Math.Min(Math.Max(posX, orbitPoint.X - radius), orbitPoint.X + radius));
            SetPositionY(Math.Max(Math.Min(posY, orbitPoint.Y + radius), orbitPoint.Y - radius));

            if (posY > (orbitPoint.Y + (radius - 0.02)))
            {
                aimAtPlayer();
                character.Fire();
            }

        }

        void aimAtPlayer()
        {
            double distanceFromPlayerX = (Game.Instance.player.Sprite.PositionX - character.Sprite.PositionX);
            double distanceFromPlayerY = (Game.Instance.player.Sprite.PositionY + Game.Instance.player.Sprite.Height / 2) + (character.Sprite.PositionY - character.Sprite.Height - 1);
            double radian = (Math.PI / 180) * (distanceFromPlayerX);
            character.Sprite.ResetSpriteRotation();
            character.Sprite.RotateSprite(-(Math.Atan2(distanceFromPlayerY, distanceFromPlayerX) * 180.0 / Math.PI - 90));
        }
    }
}
