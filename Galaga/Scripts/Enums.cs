﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galaga.Scripts
{
    public enum AiPattern
    {
        CircularRight,
        CircularLeft,
        PingPongR,
        PingPongL,
        None
    }
}
