﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace Galaga.Scripts
{
    public class Character : GameEntity
    {
        CharacterController characterController;
        BitmapImage turningLeftBitmap;
        BitmapImage turningRightBitmap;
        BitmapImage noTurningBitmap;
        double fireDelay = 0.05;
        double fireCooldown;


        public Character(Sprite sprite, CharacterController characterController) : base(sprite, true)
        {
            this.characterController = characterController;

            characterController.Initialize(this);
            fireCooldown = fireDelay;

            #region Left-turning sprite definition
            Uri turningLeftURI = new Uri("ms-appx://Galaga/Assets/shipTurningLeft.png");
            turningLeftBitmap = new BitmapImage(turningLeftURI);
            #endregion

            #region Right-turning sprite definition
            Uri turningRightURI = new Uri("ms-appx://Galaga/Assets/shipTurningRight.png");
            turningRightBitmap = new BitmapImage(turningRightURI);
            #endregion

            #region Non-turning sprite definition
            Uri noTurningURI = new Uri("ms-appx://Galaga/Assets/ship.png");
            noTurningBitmap = new BitmapImage(noTurningURI);
            #endregion
        }

        protected override void Update(double dt)
        {
            base.Update(dt);
            if (fireCooldown > 0)
                fireCooldown -= dt;
        }

        public void Fire()
        {
            if (fireCooldown <= 0)
            {
                new Bullet(this);
                fireCooldown = fireDelay;
            }
        }

        public void TurnLeft()
        {
            Sprite.UpdateSpriteImage(turningLeftBitmap);
        }

        public void TurnRight()
        {
            Sprite.UpdateSpriteImage(turningRightBitmap);
        }

        public void StopTurning()
        {
            Sprite.UpdateSpriteImage(noTurningBitmap);
        }

        private void TakeDamage(int value)
        {
//            Game.Instance.Update -= Update;
//            this.Sprite.DestroySprite();
        }

        protected override void OnCollision(GameEntity other)
        {
            base.OnCollision(other);

            if(other is Bullet)
            {
                TakeDamage(1);
            }
        }
    }
}
