﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace Galaga.Scripts
{
    public abstract class CharacterController
    {
        protected Character character;
        double moveSpeed;
        protected double maxCharacterRight;
        protected double maxCharacterTop;
        protected Point lastMoveDirection;
        protected Point CurrentMoveDirection { get; set; }
        //{
        //    get
        //    {
        //        return _currentMoveDirection;
        //    }
        //    set
        //    {
        //        _currentMoveDirection = value;
        //    }
        //}

        //void UpdateSpriteForMovement(MoveDirection newMoveDir)
        //{
        //    if (_currentMoveDirection != newMoveDir)
        //    {
        //        switch (_currentMoveDirection)
        //        {
        //            case MoveDirection.Left:
        //                character.TurnLeft();
        //                break;
        //            case MoveDirection.Right:
        //                character.TurnRight();
        //                break;
        //            case MoveDirection.None:
        //                character.StopTurning();
        //                break;
        //        }
        //    }
        //}

        public CharacterController(double moveSpeed)
        {
            this.moveSpeed = moveSpeed;
            Game.Instance.Update += Update;
            CurrentMoveDirection = new Point(0, 0);

        }

        protected virtual void Update(double dt)
        {
            if (character.Sprite == null)
                return;
            //            UpdateSpriteForMovement(lastMoveDirection);
            UpdateMove(dt);
            lastMoveDirection = CurrentMoveDirection;
        }

        public virtual void Initialize(Character character)
        {
            this.character = character;

            maxCharacterRight = Game.Instance.RenderSize.Width - character.Sprite.Width;
            maxCharacterTop = -Game.Instance.RenderSize.Height + character.Sprite.Height;
        }

        protected void SetPositionX(double xPosition)
        {
            character.Sprite.PositionX = Math.Min(Math.Max(xPosition, 0), maxCharacterRight);
        }

        protected void SetPositionY(double yPosition)
        {
            character.Sprite.PositionY = Math.Max(Math.Min(yPosition, 0),maxCharacterTop);

        }


        protected virtual void UpdateMove(double dt)
        {
            SetPositionX(character.Sprite.PositionX + (CurrentMoveDirection.X * moveSpeed * dt));
            SetPositionY(character.Sprite.PositionY - (CurrentMoveDirection.Y * moveSpeed * dt));
        }

//        void MoveLeft(double dt)
//        {
////            character.TurnLeft();
//            character.Sprite.PositionX = Math.Max(character.Sprite.PositionX - (moveSpeed * dt), 0);
//        }

//        void MoveRight(double dt)
//        {
////            character.TurnRight();
//            character.Sprite.PositionX = Math.Min(character.Sprite.PositionX + (moveSpeed * dt), maxCharacterRight);
//        }

        void RotateRight(double dt)
        {
            character.Sprite.RotateSprite(10);
        }
    }
}
