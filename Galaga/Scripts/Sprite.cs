﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Galaga.Scripts
{
    public class Sprite
    {
        Windows.UI.Xaml.Controls.Image image;
        CompositeTransform transform;
        CompositeTransform TipPosition;

        public bool ToDelete { get; set; }

        public double Rotation
        {
            get { return transform.Rotation; }
        }

        public double PositionX
        {
            get { return transform.TranslateX; }
            set { transform.TranslateX = value; }
        }

        public double TipPositionX { get; private set;}

        public double PositionY
        {
            get { return transform.TranslateY; }
            set { transform.TranslateY= value; }
        }

        public double TipPositionY { get; private set; }

        public double Width
        {
            get { return image.Width; }
        }

        public double Height
        {
            get { return image.Height; }
        }

        public string Source
        {
            get { return Source; }
            private set { }
        }

        public Sprite(double width, double height, string source, double positionX, double positionY, double rotation)
        {
            transform = new CompositeTransform();
            transform.CenterX = width / 2;
            transform.CenterY = height / 2;
            transform.Rotation = rotation;
            

            image = new Windows.UI.Xaml.Controls.Image();
            image.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            image.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Bottom;
            image.Margin = new Windows.UI.Xaml.Thickness(0);
            image.RenderTransformOrigin = new Windows.Foundation.Point(0, 0);
            image.RenderTransform = transform;
            image.Width = width;
            image.Height = height;

            TipPosition = new CompositeTransform();
            TipPosition = transform;

            // Build bitmap for image source
            source = "ms-appx://Galaga/Assets/" + source;
            Uri bitmapUri = new Uri(source);
            BitmapImage bitmap = new BitmapImage(bitmapUri);
            image.Source = bitmap;

            PositionX = positionX;
            PositionY = positionY;
            Source = source;

            Game.Instance.Grid.Children.Add(image);
            ToDelete = false;
        }

        public void UpdateSpriteImage(BitmapImage newImage)
        {
            this.image.Source = newImage;
        }

        public void DestroySprite()
        {
            ToDelete = true;
            Game.Instance.Grid.Children.Remove(this.image);
        }

        public void RotateSprite(double angle)
        {
            transform.Rotation += angle;
        }

        public void ResetSpriteRotation()
        {
            transform.Rotation = 0;
        }
    }
}